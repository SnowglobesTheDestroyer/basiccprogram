#include <stdio.h>
#include "myMath.h"

int main(int argc, char const *argv[]) {
  printf("Please insert a real number:");
  double a = 0.0;
  scanf("%lf", &a);

  double ein = sub(add(Exponent(a), Power(a, 3)), 2.0);
  double zwei = add(mul(a, 3), mul(Power(a, 2), 2));
  double drei = sub(div(mul(Power(a, 3), 4), 5), mul(a,2));

  printf("The value of f(x)= e^x + x^3 - 2   at the point %f is: %.4f\n", a, ein);
  printf("The value of f(x)= 3*x + 2*x^2     at the point %f is: %.4f\n", a, zwei);
  printf("The value of f(x)= (4*x^3)/5 - 2*x at the point %f is: %.4f\n", a, drei);
  return 0;
}
