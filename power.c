double Power (double x, int y){
  double pow = 1.0;
  if (y>0) {
    for(int i = y; 0 < i; i--){
      pow = pow * x;
    }
  }
  else{
    for(int i = y; 0 > i; i++){
      pow = pow * (1.0/x);
    }
  }
  return pow;
}

double Exponent(int x){
  return Power(2.71828,x);
}
