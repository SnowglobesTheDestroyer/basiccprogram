all : mains.o maind.o

mains.o : main.o mymaths libmyMath.a
				gcc -Wall main.o -L. -lmyMath -o mains
maind.o : main.o mymathd libmyMath.so
				gcc -Wall main.o -L. -lmyMath -o maind
main.o: main.c
				gcc -Wall -c main.c -o main.o

mymathd: libmyMath.so
mymaths: libmyMath.a
	
libmyMath.a : basicMath.o power.o myMath.h
				ar -rcs libmyMath.a basicMath.o power.o
libmyMath.so : basicMath.o power.o myMath.h
				gcc -shared -Wall basicMath.o power.o -o libmyMath.so


basicMath.o : basicMath.c
				gcc -Wall -c -fPIC basicMath.c
power.o : power.c
				gcc -Wall -c -fPIC power.c


.PHONY: clean
clean :
				rm *o edit lib*
